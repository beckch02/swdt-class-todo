cd ~
git clone beckch02@bitbucket.org:beckch02/swdt-class-todo.git
find swdt-class-todo/python/__pycache__ -name '*.pyc' -exec rm {} \;
find swdt-class-todo/python -type d -name __pycache__ -exec rm -rf {} \; 2> /dev/null
find swdt-class-todo/java -name '*.class' -exec rm {} \;
find swdt-class-todo/c -name '*.out' -exec rm {} \;
find swdt-class-todo/cpp -name '*.out' -exec rm {} \;
find swdt-class-todo/bash -name '*.txt' -exec rm {} \;
cd swdt-class-todo
git add .
git commit -m "Cleaned the repository"
git push
cp ~/cleanscript.sh .
git add cleanscript.sh
git commit -m "Adding script"
git push 
